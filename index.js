console.log("Hello");

// Initialiazation Notation

let trainer = {
	name: 'Ash Ketchum',
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Brock'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(index){
		console.log(">> " + this.pokemon[index] + "! I choose you!");
	}
}

console.log("\nResult of Dot Notation");
console.log(">> " + trainer.name);

console.log("\nResult of Square Bracket Notation");
console.log(trainer['pokemon']);

console.log("\nResult of Talk Method");
for(let i = 0; i < 4; ++i){
	trainer.talk(i);
}




// Constructor

function Pokemon(name, level){

	//Properties

	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;


	// Methods

	this.tackle = function(target){
		console.log(">> " + this.name + ' tackled ' + target.name);
		target.health = target.health - this.attack;
		console.log(">> " + target.name + "'s health is now reduced to " + target.health);
		if(target.health <= 0){
			target.faint();
		}
	};

	this.faint = function(){
		console.log(">> " + this.name + ' fainted ');
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log("\nDisplaying objects instantiated by constructor");
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

console.log("\nPikachu attacking Geodude");
pikachu.tackle(geodude);

console.log("\nPikachu attacking Mewtwo");
pikachu.tackle(mewtwo);

console.log("\nGeodude attacking Pikachu");
geodude.tackle(pikachu);

console.log("\nGeodude attacking Mewtwo");
geodude.tackle(mewtwo);

console.log("\nMewtwo attacking Pikachu");
mewtwo.tackle(pikachu);

console.log("\nMewtwo attacking Geodude");
mewtwo.tackle(geodude);

console.log("\nDisplaying objects properties after battle royale");
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);